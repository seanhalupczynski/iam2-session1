#! /usr/bin/python3

vowel_words = []
consonant_words = []
#word_file = open('/home/shalupczynski/Desktop/Infrastructure_Engineer_Academy/iam2-session1/week2/wordlist.txt').read().split(' ')
with open('../week2/wordlist.txt') as wordfile:
    word_list = list(wordfile)
for line in word_list:
    line = line.strip()
    #print(line[0][0])
    if line[0].lower() in 'aeiou':
        vowel_words.append(line)
    else:
        consonant_words.append(line)

print(sorted(consonant_words))
print(sorted(vowel_words))
