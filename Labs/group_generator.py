#! /usr/bin/python3
import random

class_list = ['James Brett', 'Calvin Demerath', 'Sean Halupczynski', 'Colin Harper', 'Joseph Imprescia', 'Anthony LoPresti', 'William, Mayo', 'Patrick MaCardy', 'Craig Norton', 'Daniel Pabian', 'Billy Raimey', 'Andres Sanchez', 'David Spera', 'Barbara Walton', 'Andrea Warren']

people_in_group = int(input("How many individuals in a group? "))

group = []

num_of_groups = len(class_list) // people_in_group

extra_people = len(class_list) % people_in_group

while len(class_list) > extra_people:
    random_index = random.randint(0, len(class_list) - 1)
    group.append(class_list[random_index])
    if len(group) == people_in_group:
        print(group)
        group = []
    class_list.remove(class_list[random_index])

print("Extra people", class_list)


#i = 0

#for i in range (0, num_of_groups):
#    print(i)

#for person in class_list:
#    group.append(person)
#    print(group)


