#!/usr/bin/bash

file=$1

if [ -z $file ]
then
	echo "You didn't give me a file to check."
	exit 1
fi
for item in "$@"
do
	if [ ! -e $item ]
	then
		echo "$item does not exist."
	else
		echo "$item exists."
		#if [ $(ls -l $item | awk '{print $6}') -eq 0 ] This does the same as the inverse of -s but long hand
		if [ ! -s $item ]
		then
			echo "Removing empty file $item"
			rm $item
		fi
		echo "Inspected by Sean." >> $item
	fi
done
