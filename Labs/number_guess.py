#! /usr/bin/python3
import random

user_num = int(input("Guess a number from 1 to 100 [you can quit with 0]: "))

comp_num = random.randint(1, 100)

guesses = []

while not user_num == comp_num:
    if user_num == 0:
        print("Sorry you gave up.")
        break
    guesses.append(user_num)
    if user_num > comp_num:
        print("You are too high.")
        print("Your guesses:", guesses)
    else:
        print("You are too low.")
        print("Your guesses:", guesses)
    user_num = int(input("Guess another number from 1 to 100 [you can quit with 0]: "))
else:
    print("You got it!")
    print("The random number was", comp_num)


