#! /usr/bin/python3

#user_input = input("Type away: ").split()
word_dictionary = {}
letters_dictionary = {}
punctuation = '%%$@_$^__#)^)&!_+]!*@&^}@[@%]()%+$&[(_@%+%$*^@$^!+]!&_#)_*}{}}!}_]$[%}@[{_@#_^{*'
with open('chars.txt') as hamlet:
    for line in hamlet:
        words = line.strip().upper().split()
        for word in words:
            #chars = word.split() 
            word = word.strip(punctuation)
            #for char in chars:
            if word_dictionary.get(word):
                word_dictionary[word] += 1
            else:
                word_dictionary[word] = 1

for key in sorted(word_dictionary, key=word_dictionary.get, reverse=True):
    print(key ,"=>", word_dictionary[key])
