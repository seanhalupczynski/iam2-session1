#! /usr/bin/python3

#user_input = input("Type away: ").split()
word_dictionary = {}
keep_typing = True

while keep_typing:
    user_input = input("Type away: ").split()
    for word in user_input:
        for punc in word:
            if punc in '.?,|:;\/!()&':
                list_word = list(word)
                list_word.remove(punc)
                word = ''.join(list_word)
        if word_dictionary.get(word):
            word_dictionary[word] += 1
        else:
            word_dictionary[word] = 1
    if not input("Keep typing [y/n]? ") == "y":
        keep_typing = False

print(word_dictionary)


