#! /usr/bin/python3
num = int(input("Enter a number: "))
fact = 1

for n in range(1, num + 1):
    fact *= n

print(fact)

#Other option for the for loop. Decriments by 1.
#Starts at the user input and ends at 0 (not including 0) and adding a -1 (subtracting by 1) each iteration.

#for n in range(num, 0, -1)
#   fact *= n
