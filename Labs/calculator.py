#! /usr/bin/python

def add(a,b):
    result = a + b
    print(result)
    return result

def subtract(a,b):
    result = a - b
    print(result)
    return result

def multiply(a,b):
    result = a * b
    print(result)
    return(result)

def divide(a,b):
    result = float(a) / float(b)
    print(result)
    return(result)

def power(a, b):
    result = a ** b
    print(result)
    return(result)

def root(a, b):
    result = a ** 1/b
    print(result)
    return(result)

total = 0

def get_operator():
    return raw_input("What would you like to do [add, subtract, multiply, divide, power, root]? ").strip()

def get_num1():
    return float(raw_input("Enter a number: "))

def get_num2():
    return float(raw_input("Enter another number: "))

#def more_math():
#    return raw_input("Would you like to do more math [y/n]? ").strip()

def do_math(c, a, b):

    while not c in ('add', 'subtract', 'multiply', 'divide', 'power', 'root'):
        print("That opperation is not valid.")
        c = get_operator()

    if c == 'add':
        total = add(a, b)
    elif c == 'subtract':
        total = subtract(a, b)
    elif c == 'multiply':
        total = multiply(a, b)
    elif c == 'divide':
        total = divide(a, b)
    elif c == 'power':
        total = power(a, b)
    elif c == 'root':
        total = root(a, b)
    
    more_math = raw_input("Would you like to do more math to your total [y/n]? ").strip()

    while not more_math in ('y', 'n'):
        print("Please enter y for Yes or n for No.")
        more_math = raw_input("Would you like to do more math to you total [y/n]? ").strip()

    if more_math == 'y'.strip():
        do_math_again(total)
    else: 
        start_new_math()
    
def do_math_again(t):
    do_math(get_operator(), t, get_num2())

def start_new_math():
    new_math = raw_input("Do you want to start new math [y/n]? ").strip()
    while not new_math in ('y', 'n'):
        print("Please enter y for Yes or n for No.")
        new_math = raw_input("Do you want to start new math [y/n]? ").strip()
    
    if new_math == 'y'.strip():
        total = 0
        do_math(get_operator(), get_num1(), get_num2())
    else:
        print("Goodbye")

do_math(get_operator(), get_num1(), get_num2())

