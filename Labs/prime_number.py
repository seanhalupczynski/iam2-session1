#! /usr/bin/python3

start = int(input("Enter a starting number: "))
end = int(input("Enter an ending number: "))


if start <= end:
    for i in range(start, end + 1):
        is_prime = True
        for num in range(2, i):
            if i % num == 0:
                print(f'{i} is not prime. {num} divides evenly into {i}.')
                is_prime = False
                continue
        else:
            if is_prime:
                print(f'{i} is a prime number.')
else:
    for a in range(start, end + 1, -1):
        is_prime = True
        for numb in range(2, a):
            if a % numb == 0:
                print(f'{a} is not prime. {numb} divides evenly into {a}.')
                is_prime = False
                continue
        else:
            if is_prime:
                print(f'{a} is a prime number.')
                
