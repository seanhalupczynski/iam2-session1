#! /usr/bin/python3
import re

with open('pythonchallenge_level4.txt') as text:
    txt = []
    for line in text:
        txt.append(line.strip())
    str_txt = ''.join(txt)
    
    special_txt = ''.join(re.findall("[a-z]{2}[A-Z]{3}[a-z][A-Z]{3}[a-z]{2}", str_txt))
    special_lower_txt = ''.join(re.findall("[a-z]", special_txt))
    print(special_lower_txt)
