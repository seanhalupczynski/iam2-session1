#! /usr/bin/python3
import random

words_list = ['python', 'java', 'javascript', 'html', 'css', 'bootstrap', 'ruby', 'sql', 'mongo', 'node', 'express', 'react', 'angular', 'linux', 'unix', 'bash', 'groovy', 'maven', 'chef', 'ansible']
wins = 0
losses = 0
keep_playing = True

while keep_playing:
    random_word = random.choice(words_list)
    random_word_as_list = list(random_word)
    random.shuffle(random_word_as_list)
    jumbled_word = ''.join(random_word_as_list)
    number_of_guesses = 3
#    scoreboard = f'Scoreboard: Wins - {wins} Losses -{losses}'

    print(jumbled_word)

    while number_of_guesses > 0:
        print(f'You have {number_of_guesses} guesses.')
        guess_message = input('What is your guess? ')
        if guess_message == random_word:
            print("You got it.")
            wins += 1
            print('Scoreboard:', 'Wins -', wins, 'Losses -', losses)
            if input("Keep playing [y/n]? ") == 'y': 
                keep_playing = True
            else: 
                keep_playing = False
            break
        number_of_guesses -= 1
    else:
        print(f'You did not guess the word. It was {random_word}.')
        losses += 1
        print('Scoreboard:', 'Wins -', wins, 'Losses -', losses)
        if input("Keep playing [y/n]? ") == 'y': 
            keep_playing = True
        else: 
            keep_playing = False
else:
    print("Thanks for playing!")
