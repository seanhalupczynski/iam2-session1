#! /usr/bin/python3

roman_numeral = {
        "I" : 1,
        "V" : 5,
        "X" : 10,
        "L" : 50,
        "C" : 100,
        "D" : 500,
        "M" : 1000
        }
user_input = input("Pass in a Roman Numeral: ").upper()
total = 0


for num in user_input:
    if user_input.index(num) < len(user_input) - 1:
        if roman_numeral[num] >= roman_numeral[user_input[user_input.index(num) + 1]]:
            total += roman_numeral[num]
        else:
            total += (roman_numeral[user_input[user_input.index(num) +1]] - roman_numeral[num])
    else:
        if roman_numeral[num] > roman_numeral[user_input[user_input.index(num) -1]]:
            break
        else:
            total += roman_numeral[num]

print(total)

#Shorter, better way to handle it.
#Keep variables roman_numeral, user_input, and total.
#prev_value = 0 Setting previous value to 0
#for num in user_input:
#   total += value Adds value regardless
#   if value > prev_value: Checks if the current value is larger than the previous
#       total -= value * 2 Subtract twice the value to account for already adding it to the total once.
#   prev_value = value Set the previous value to the current value before going into the next iteration
#print(total)
